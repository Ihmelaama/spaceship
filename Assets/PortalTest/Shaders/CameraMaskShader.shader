Shader "Custom/Camera Mask" {

	Properties {
	_MainTex ("Texture", 2D) = "white" {}
	_X1 ("X1", Range (0.0, 1.0)) = 0.5
  _Y1 ("Y1", Range (0.0, 1.0)) = 0.5  
  _X2 ("X2", Range (0.0, 1.0)) = 0.1  
  _Y2 ("Y2", Range (0.0, 1.0)) = 0.1
  }
  
SubShader {
	Pass {
		ZTest Always Cull Off ZWrite Off
				
  CGPROGRAM
  #pragma vertex vert
  #pragma fragment frag
  #include "UnityCG.cginc"
  
  uniform sampler2D _MainTex;
  uniform float _X1;
  uniform float _Y1;  
  uniform float _X2;
  uniform float _Y2;  

  struct v2f {
  float4 pos : SV_POSITION;
  float2 uv : TEXCOORD0;
  };

  v2f vert(appdata_img v) {
  v2f o;
  o.pos = UnityObjectToClipPos (v.vertex);
  o.uv = v.texcoord;
  return o;
  }
  
  float4 frag(v2f i) : SV_Target {
    
    float2 p = i.uv;

    if(p.x<_X1 || p.x>_X2 || p.y<_Y1 || p.y>_Y2) discard;

    return tex2D(_MainTex, p); 
  
  }

ENDCG

}
}

Fallback off

}
