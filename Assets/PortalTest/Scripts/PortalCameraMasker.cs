using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class PortalCameraMasker : MonoBehaviour {

  [Range(0f, 1f)]
  public float x1=0f;
  
  [Range(0f, 1f)]
  public float y1=0f;
  
  [Range(0f, 1f)]
  public float x2=1f;
  
  [Range(0f, 1f)]
  public float y2=1f;  
  
	public Shader shader;
	private Material material;
 
	void Awake() {
	if(shader!=null) material=new Material(shader);
	}
  
  void Update() {

    material.SetFloat("_X1", x1);
    material.SetFloat("_Y1", y1);

    material.SetFloat("_X2", x2);
    material.SetFloat("_Y2", y2);

  }

	void OnRenderImage(RenderTexture source, RenderTexture destination) {	
	
    if(shader==null) return;
    if(material==null) material=new Material(shader);
    if(material==null) return;
    Graphics.Blit(source, destination, material);
  
	}
  
}