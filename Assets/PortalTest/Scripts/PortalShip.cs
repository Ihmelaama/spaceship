using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PortalShip : MonoBehaviour {

//----------------------------------------------------
// VARIABLES

  // settings ---

    public GameObject player;
    public List<GameObject> roomPrefabs;
    public int numRooms=5;
    
  // holders ---
  
    private List<PortalRoom> rooms=new List<PortalRoom>();

//----------------------------------------------------
// EVENTS

	void Start() {
  
    createRoom();

	}
  
//------------

	void Update() {
		
	}

//----------------------------------------------------
// PUBLIC SETTERS

//----------------------------------------------------
// PRIVATE SETTERS

  private void createRoom() {
  
    int rand=Random.Range(0, roomPrefabs.Count);
    GameObject g=Instantiate(roomPrefabs[rand]);
    
    g.transform.SetParent(transform);
    g.transform.localPosition=Vector3.zero;
       
    PortalRoom r=g.GetComponent<PortalRoom>();
    
    if(rooms.Count==0) {
    r.spawnPlayer(player);    
    r.setAsCurrent();
    }
    
    rooms.Add(r);
  
  }
  
}
