using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PortalPlayer : MonoBehaviour {

//----------------------------------------------------
// VARIABLES

  public static PortalPlayer instance;

//----------------------------------------------------
// EVENTS

  void Awake() {
  instance=this;
  }
  
//------------

	void Start() {
		
	}
  
//------------
	
	void Update() {
		
	}

//------------
  
  void OnTriggerEnter(Collider c) {
  
    PortalRoom[] r=c.gameObject.GetComponentsInParent<PortalRoom>();
    
    if(r.Length>0) {
    r[0].setAsCurrent();
    }
    
  }
  
}
