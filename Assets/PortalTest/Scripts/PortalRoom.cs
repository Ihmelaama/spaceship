using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PortalRoom : MonoBehaviour {

//----------------------------------------------------
// VARIABLES

  // settings ---
  
    public bool firstRoom=false;

  // holders ---
  
    private Transform[] childTransforms;
  
    private Camera roomCamera;  
    private PortalCameraMasker cameraMasker;
    
    private List<GameObject> entrances=new List<GameObject>();
    private Transform playerSpawn;

  // state ---
  
    public static PortalRoom currentRoom=null;

//----------------------------------------------------
// EVENTS

  void Start() {
  
    Transform[] t=GetComponentsInChildren<Transform>();
    GameObject g;
    BoxCollider b;
    Vector3 v;
    
    childTransforms=t;
    
  // init ---
  
    if(roomCamera!=null) cameraMasker=roomCamera.GetComponent<PortalCameraMasker>();
    if(firstRoom) setAsCurrent();
    
    for(int i=0; i<t.Length; i++) {

    // get player spawn ---
    
      if(t[i].name=="PlayerSpawn") {
      playerSpawn=t[i];
      }

    // create entrance ---
    
      if(t[i].name.IndexOf("Entrance")>-1) {
      
        g=t[i].gameObject;
        g.GetComponent<Renderer>().enabled=false;

        b=g.AddComponent<BoxCollider>();
        
        v=b.center;
        v.z=1f;
        b.center=v;
        
        v=b.size;
        v.z=2f;
        b.size=v;

        entrances.Add(g);

      }
    
    }
    
  // create camera ---

    if(!roomCamera) createRoomCamera();
   
  }
  
//------------
	
	void Update() {
  
    if(this!=PortalRoom.currentRoom) {
    maskCamera();
    }

	}
  
//----------------------------------------------------
// PUBLIC SETTERS  

  public void spawnPlayer(GameObject g) {
  
    if(playerSpawn!=null) {
  
      g.transform.position=playerSpawn.position;
      g.transform.rotation=playerSpawn.rotation;
      
      roomCamera=g.transform.Find("MainCamera").GetComponent<Camera>();

    }
  
  }
  
//------------

  public void setAsCurrent() {
  
    if(this==PortalRoom.currentRoom) return;
  
    PortalRoom prev=PortalRoom.currentRoom;
    if(prev) prev.roomCamera.depth=0;
    
    PortalRoom.currentRoom=this;
    if(!roomCamera) createRoomCamera();
    roomCamera.depth=1;
    unmaskCamera();

  }
  
//----------------------------------------------------
// PRIVATE SETTERS   

  private void createRoomCamera() {

    if(!roomCamera) {
  
      roomCamera=Instantiate(PortalWorldSettings.instance.playerCamera);
      roomCamera.enabled=true;
      roomCamera.transform.SetParent(PortalPlayer.instance.transform);
      roomCamera.transform.localPosition=Vector3.zero;
      roomCamera.transform.rotation=Quaternion.identity;
      roomCamera.cullingMask = 1 << gameObject.layer ;  
  
    }
  
  }

//------------
  
  private void maskCamera() {

  }
  
//------------

  private void unmaskCamera() {
  
    if(cameraMasker!=null) {
    
      cameraMasker.x1=0;
      cameraMasker.y1=0;
      cameraMasker.x2=1;
      cameraMasker.y2=1;
        
    }
  
  }
  
}
                                                