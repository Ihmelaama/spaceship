using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMove : MonoBehaviour {

// rotation ---

  private float rotationSpeed=0.05f;
  private float rollSpeed=0.015f;
  
  private float rotationFriction=0.92f;
  private float rollFriction=0.96f;

  private Vector3 rotationVelocity=Vector3.zero;
  private float rollVelocity=0f;
    
// move ---
   
  private float forwardSpeed=0.001f;  
  private float strafeSpeed=0.0007f;
  private float verticalSpeed=0.0003f;  
  
  private float forwardFriction=0.92f;  
  private float strafeFriction=0.92f;
  private float verticalFriction=0.95f;  

  private float forwardVelocity=0f;     
  private float strafeVelocity=0f;      
  private float verticalVelocity=0f;    

	void Start() {
		
	}
	
	void Update() {
  
    bool b1;
    bool b2;
    
  // horizontal rotation ---
    
    b1=Input.GetKey(KeyCode.LeftArrow);
    b2=Input.GetKey(KeyCode.RightArrow);
    
    if(b1) rotationVelocity.y-=rotationSpeed; 
		if(b2) rotationVelocity.y+=rotationSpeed;
    if(!b1 && !b2) rotationVelocity.y*=rotationFriction;
    
  // vertical rotation ---
    
    b1=Input.GetKey(KeyCode.UpArrow);
    b2=Input.GetKey(KeyCode.DownArrow);
        
    if(b1) rotationVelocity.x-=rotationSpeed;
    if(b2) rotationVelocity.x+=rotationSpeed;
    if(!b1 && !b2) rotationVelocity.x*=rotationFriction;
    
    transform.rotation=transform.rotation*Quaternion.Euler(rotationVelocity);
    
  // roll ---
  
    b1=Input.GetKey(KeyCode.Q);
    b2=Input.GetKey(KeyCode.E);
    
    if(b1) rollVelocity+=rollSpeed;
    if(b2) rollVelocity-=rollSpeed;
    if(!b1 && !b2) rollVelocity*=rollFriction;
    
    transform.rotation=transform.rotation*Quaternion.Euler(new Vector3(0f, 0f, 1f)*rollVelocity);

  // move forward and back ---
  
    b1=Input.GetKey(KeyCode.W);
    b2=Input.GetKey(KeyCode.S);
    
    if(b1) forwardVelocity+=forwardSpeed;
    if(b2) forwardVelocity-=forwardSpeed;
    if(!b1 && !b2) forwardVelocity*=forwardFriction;
    
    transform.position=transform.position+transform.forward*forwardVelocity;
    
  // strafe ---
  
    b1=Input.GetKey(KeyCode.A);
    b2=Input.GetKey(KeyCode.D);
    
    if(b1) strafeVelocity+=strafeSpeed;
    if(b2) strafeVelocity-=strafeSpeed;
    if(!b1 && !b2) strafeVelocity*=strafeFriction;
    transform.position=transform.position-transform.right*strafeVelocity;
    
  // up and down  ---
  
    b1=Input.GetKey(KeyCode.R);
    b2=Input.GetKey(KeyCode.F);
    
    if(b1) verticalVelocity+=verticalSpeed;
    if(b2) verticalVelocity-=verticalSpeed;
    if(!b1 && !b2) verticalVelocity*=verticalFriction;
    transform.position=transform.position+transform.up*verticalVelocity;

    transform.position=transform.position+transform.up*verticalVelocity; 
 
	}
  
}
