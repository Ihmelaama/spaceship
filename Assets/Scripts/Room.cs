using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Room : MonoBehaviour {

//---------------------------------------------------------
// VARIABLES

  // entrances ---
  
    [Header("Entrances:")]

    [Tooltip("positive Y axis")]
    public bool up=true;
    
    [Tooltip("negative Y axis")]
    public bool down=true;
  
    [Tooltip("negative X axis")]
    public bool left=true;
    
    [Tooltip("positive X axis")]
    public bool right=true;
    
    [Tooltip("positive Z axis")]
    public bool forward=true;
    
    [Tooltip("negative Z axis")]
    public bool back=true;
    
  // state ---

    [System.NonSerialized]
    public List<Vector3> conn=new List<Vector3>();

    public string id=null;
    
//---------------------------------------------------------
// EVENTS

	void Awake() {
  Init();
	}
  
//------------
	
	void Update() {
		
	}
  
//---------------------------------------------------------
// PUBLIC SETTERS

  public void Init() {
  
    if(conn.Count==0) {
    
      Vector3 v=Vector3.one*10f;
      conn.Add( up ? Vector3.up : v );
  	  conn.Add( down ? Vector3.down : v );
      conn.Add( left ? Vector3.left : v );
      conn.Add( right ? Vector3.right : v );
      conn.Add( forward ? Vector3.forward : v );
      conn.Add( back ? Vector3.back : v );

    }

  }

}
