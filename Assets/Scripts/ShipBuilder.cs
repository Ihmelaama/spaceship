using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShipBuilder : MonoBehaviour {

//---------------------------------------------------------
// VARIABLES

  // settings ---

    public int numRooms=5;
    
    public List<Room> roomPrefabs=new List<Room>();
    
    public float roomSize=3f;
    
    public Camera playerCamera;

  // holders ---
  
    private List<Room> rooms=new List<Room>();

//---------------------------------------------------------
// EVENTS

	void Start() {
  
    bool b=true;
    while(b && rooms.Count<numRooms) {
    b=createRoom();
    }
    
    spawnPlayer();
		
	}
  
//------------

	void Update() {
		
	}
  
//---------------------------------------------------------
// PUBLIC SETTERS  
  
//---------------------------------------------------------
// PRIVATE SETTERS  

  private void spawnPlayer() {
  
    Room r;
    if(rooms.Count>=0 && playerCamera!=null) {
    
      r=rooms[0];
      Transform s=findChild("PlayerSpawn");     
      
      if(s!=null) {
      playerCamera.transform.position=s.position;
      //playerCamera.transform.rotation=s.rotation;
      }
      
    }
  
  }
  
//---------
  
  private bool createRoom() {
  
    int rand;
    bool b=false;
    
    rand=Random.Range(0, roomPrefabs.Count);
    Room room=Instantiate(roomPrefabs[rand]);

    if(rooms.Count>0) {
    
      int i=0;
      while(!b) {

        rand=Random.Range(0, rooms.Count);
        b=connectRooms(rooms[rand], room);

        if(!b) {
        Destroy(room.gameObject);
        rand=Random.Range(0, roomPrefabs.Count);
        room=Instantiate(roomPrefabs[rand]);        
        }
        
        i++;
        if(i>1000) break;
      
      }

      if(b) room.name+=" / num "+(rooms.Count+1);
          
    } else {
    
      b=true;
      room.name+=" / First room";
      room.transform.localPosition=Vector3.zero;
      room.transform.SetParent(transform);
    
    }
    
    if(b) {
    
      rooms.Add(room);
    
    } else {
    
      Debug.Log("can't do it, sorry");
      Destroy(room.gameObject);
      return false;
    
    }

  return true;
  }

//------------

  private bool connectRooms(Room oldRoom, Room newRoom) {
  
    oldRoom.Init();
    newRoom.Init();

    //if(oldRoom.id==newRoom.id) return false;
  
    int i, j;
    List<int> oldConn=new List<int>();
    List<int> newConn=new List<int>();

    for(i=0; i<oldRoom.conn.Count; i++) {
    
      for(j=0; j<newRoom.conn.Count; j++) {
      
        if(oldRoom.conn[i]+newRoom.conn[j]==Vector3.zero) {
        
          oldConn.Add(i);
          newConn.Add(j);
        
        }
      
      }
    
    }
    
    if(oldConn.Count>0) {
    
      int rand=Random.Range(0, oldConn.Count-1);
      i=oldConn[rand];
      j=newConn[rand];
      
      Vector3 oldDir=oldRoom.conn[i];
      Vector3 newDir=newRoom.conn[j];
      
      Vector3 pos=oldRoom.transform.localPosition;
      pos+=oldDir*roomSize;
      newRoom.transform.localPosition=pos;
      
      oldRoom.conn[i]=Vector3.one*10f;
      newRoom.conn[j]=Vector3.one*10f;
      
      newRoom.transform.SetParent(transform);
      
      return true;
    
    }
    
  return false;
  }
  
//---------------------------------------------------------
// HELPFUL

  private Transform findChild(string name) {
  
    Transform[] t=GetComponentsInChildren<Transform>();
    for(int i=0; i<t.Length; i++) {
    if(t[i].name==name) return t[i];
    }
  
  return null;
  }

}
      